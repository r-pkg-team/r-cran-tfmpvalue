Source: r-cran-tfmpvalue
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-tfmpvalue
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-tfmpvalue.git
Homepage: https://cran.r-project.org/package=TFMPvalue
Rules-Requires-Root: no

Package: r-cran-tfmpvalue
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R P-Value Computation for Position Weight Matrices
 In putative Transcription Factor Binding Sites (TFBSs) identification
 from sequence/alignments, it is interesting to obtain the significance
 of certain match score. TFMPvalue provides the accurate calculation of
 P-value with score threshold for Position Weight Matrices, or the score
 with given P-value. This package is an interface to code originally made
 available by Helene Touzet and Jean-Stephane Varre, 2007, Algorithms Mol
 Biol:2, 15.
